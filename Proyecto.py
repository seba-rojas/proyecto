import random
from os import system
from time import sleep
Speed = 1

# Función crea el largo de la matriz de forma aleatoria
def fil_col():
    N = random.randint(8,20)
    lista = []
    for i in range(N):
        a = i
        lista.append(a)      
    return lista

#Funcion rellena cada espacio de la matriz con " "
def inicializacion(B):
     relleno = []
     for num in B:
        indice = []
        for fil in range(len(B)):
            dato = " "
            indice.append(dato)
        relleno.append(indice)
     return relleno

#Imprime la matriz en la terminal
def imprimir(a, B):
    largo = len(a)
    texto1 = "  "
    #Si la columna es menor que 8, imprime un espacio más adelante del 1 al 9
    for col in range(largo):
        if col > 8:
            texto1 = f"{texto1}  {a[col]+1}"
        else:
            texto1 = f"{texto1}  {a[col]+1} "
    print(texto1)

    for num in range(largo):
        #Si la fila es menor que 9, imprime un espacio más adelante del 0 al 8 según los indeces de la matriz
        if num < 9 :
          texto = f" {a[num]+1}|"
        else:
            texto = f"{a[num]+1}|"
        #Luego se le imprime el contenido según el indice que sea igual a la fila  
        for num2 in B[num]:
            texto = f"{texto} {num2} |"
        print(texto)

#Función que le asigna la posición de la particula en la matriz, de forma aleatoria
def particula(a, B, F, C):
    B[F][C]= "*"
    return B

#Función que le asigna a una posición de forma aleatoria , sin que se le asigne en un espacio ya ocupado 
#por una letra anteriormente
def Letras(a,B,F,C):

    paso = 1
    i = 0
    # Ciclo que dura hasta que todas las letras esten en la matriz 
    while paso != 0 :
        # Condicion que le asignara la letra A en la matriz las veces que sean necesarias
        if paso == 1:
            veces = random.randint(1,7)
            while i < veces:
            #Se elije una posicion de forma aleatoria 
                x = random.randint(0,len(a)-1)
                y = random.randint(0,len(a)-1)
                #Si esta posicion esta vacia se le asigna la letra A 
                if B[x][y] == " ":
                    B[x][y] = "A"
                    i += 1 
                    paso = 2
        # Condicion que le asignara la letra S en la matriz las veces que sean necesarias
        elif paso == 2:
            i = 0
            veces = random.randint(1,7)
            while i < veces:
            #Se elije una posicion de forma aleatoria 
                x = random.randint(0,len(a)-1)
                y = random.randint(0,len(a)-1)
                 #Si esta posicion esta vacia se le asigna la letra S
                if B[x][y] == " ":
                    B[x][y] = "S"
                    i += 1 
                    paso = 3
        # Condicion que le asignara la letra E en la matriz las veces que sean necesarias
        elif paso == 3:
            i = 0
            veces = random.randint(1,7)
            while i < veces:
            #Se elije una posicion de forma aleatoria 
                x = random.randint(0,len(a)-1)
                y = random.randint(0,len(a)-1)
                 #Si esta posicion esta vacia se le asigna la letra E 
                if B[x][y] == " ":
                    B[x][y] = "E"
                    i += 1 
                    paso = 4 
        # Condicion que le asignara la letra B en la matriz las veces que sean necesarias  
        elif paso == 4:
            i = 0
            veces = random.randint(1,7)
            while i < veces:
            #Se elije una posicion de forma aleatoria 
                x = random.randint(0,len(a)-1)
                y = random.randint(0,len(a)-1)
                 #Si esta posicion esta vacia se le asigna la letra B
                if B[x][y] == " ":
                    B[x][y] = "B"
                    i += 1 
                    paso = 5
        # Condicion que le asignara la letra F en la matriz las veces que sean necesarias
        elif paso == 5:
            i = 0
            veces = 2
            while i < veces:
            #Se elije una posicion de forma aleatoria 
                x = random.randint(0,len(a)-1)
                y = random.randint(0,len(a)-1)
                 #Si esta posicion esta vacia se le asigna la letra F
                if B[x][y] == " ":
                    B[x][y] = "F"
                    i += 1 
                    paso = 0
    return B         

#Función que nos entrega y guarda la posición de la particula
def PosParticula(a,B,F,C):
    for j in range(len(a)):
        for k in range(len(a)-1):
            if B[j][k] == "*":
                x = j
                y = k
    return x, y

#Las siguientes 8 funciones definen los movimientos o direcciones
#de la particula a traves de la matriz
#Lo descrito en lo que ocurre con cada letra en la siguiente funcion, ocurre lo mismo con todas las funciones de direccion

#Función que mueve la particula a la izquirda de forma horizontal y revisa si la siguiente casilla es una letra o no
def Horizontal_Izquierda(a, B, F, C,Pisadas):
    for j in range(len(a)):
        for k in range(len(a)-1,0,-1):
        #Se revisa la posicion actal y la siguente si esta esta vacia la particula se movera a esa posicion 
            if B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == " ":
                B[F][k-1] = "*"
                B[F][k] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                x = F
                y = k - 1
                Pisadas += 1
                #si la siguiente posicion es una A esta acuatuara como una pared y la particula rebotara en tres
                #posibles direcciones
            elif B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == "A":
                Direc = random.randint(1,3)
                #Se elije una direccion de manera aleaotoria 
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
                    #Si la siguiente posicion es una B esta se correra una posicion mas a la direccion que fue empujada 
                    #y la particula rebotara nuevamente en direcciones aleatoria 
            elif B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == "B":
                B[j][k-2] = "B"
                B[j][k-1] = " "
                Direc = random.randint(1,3)
                #Se elije una direccion de forma aleatoria
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
                    #Si la siguiente posicion es una S la particula pasara a tener una especie de superpoder el cual puede eliminar 
                    #de forma momentanea cualquier letra que toque en un rango de 10 pasos
            elif B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == "S":
                B[F][k-1] = "*"
                B[F][k] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                x = F
                y = k - 1
                Pisadas += 1
                ##Si la siguiente posicion es una E la particula reaparecera en la posicion (0,0) de la matriz y esta letra desaparecera momentaneamente 
            elif B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == "E":
                B[j][k-1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            #Si la siguiente posicion es una F el juego simplemente termina dando las gracias al jugador por participar
            elif B[j][k] == "*" and B[j][0] != "*" and B[j][k-1] == "F":
                Pisadas = 5000
            else:
                x = F 
                y = 0
    return x,y,Pisadas
#Función que mueve la particula a la derecha de forma horizontal y revisa si la siguiente casilla es una letra o no
def Horizontal_Derecha(a, B, F, C,Pisadas):
    for j in range(len(a)):
        for k in range(len(a)-1):
            if B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] == " ":
                B[F][k+1] = "*"
                B[F][k] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                x = F
                y = k + 1
                Pisadas += 1
            elif B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] == "A":
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] == "B":
                B[j][k+2] = "B"
                B[j][k+1] = " "
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] == "S":
                B[F][k+1] = "*"
                B[F][k] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                x = F
                y = k + 1
                Pisadas += 1
            elif B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] == "E":
                B[j][k+1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            elif B[j][k] == "*" and B[j][len(a)-1] != "*" and B[j][k+1] != "F":
                Pisadas = 5000
            else:
                x = F 
                y = len(a)-1
    return x,y,Pisadas

#Función que mueve la particula hacia arriba de forma vertical y revisa si la siguiente casilla es una letra o no
def Vertical_Arriba(a, B, F, C,Pisadas):

    for j in range(len(a)-1,0,-1):
        if B[j][C] == "*" and B[j-1][C] == " ":
            B[j-1][C] = "*"
            B[j][C] = " "
            imprimir(a,B)
            sleep(Speed)
            system("cls")
            x = j - 1 
            y = C
            Pisadas += 1
        elif B[j][C] == "*" and B[j-1][C] == "A":
            Direc = random.randint(1,3)
            if Direc == 1:
                x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif Direc == 2:
                x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
        elif B[j][C] == "*" and B[j-1][C] == "B":
            B[j-2][C] = "B"
            B[j-1][C] = " "
            Direc = random.randint(1,3)
            if Direc == 1:
                x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif Direc == 2:
                x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
        elif B[j][C] == "*" and B[j-1][C] == "S":
            B[j-1][C] = "*"
            B[j][C] = " "
            imprimir(a,B)
            sleep(Speed)
            system("cls")
            x = j - 1 
            y = C
            Pisadas += 1
        elif B[j][C] == "*" and B[j-1][C] == "E":
            B[j-1][C] = " "
            T = random.randint(0,1)
            if T == 0:
                B[j][C] = " "
                B[0][0] = "*"
                x = 0
                y = 0
                return x,y,Pisadas
            elif T == 1:
                B[j][C] = " "
                B[len(a)-1][len(a)-1] = "*"
                x = len(a)-1
                y = len(a)-1
                return x,y,Pisadas
        elif B[j][C] == "*" and B[j-1][C] == "F":
            Pisadas = 5000
        else:
                x = 0 
                y = C  
    return x,y,Pisadas

#Función que mueve la particula hacia abajo de forma vertical y revisa si la siguiente casilla es una letra o no
def Vertical_Abajo(a, B, F, C,Pisadas):
    for j in range(len(a)-1):
        if B[j][C] == "*" and B[j+1][C] == " ":
            B[j+1][C] = "*" 
            B[j][C] = " "
            imprimir(a,B)
            sleep(Speed)
            system("cls")
            x = j + 1 
            y = C
            Pisadas += 1
        elif B[j][C] == "*" and B[j+1][C] == "A":
            Direc = random.randint(1,3)
            if Direc == 1:
                x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
            elif Direc == 2:
                x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
        elif B[j][C] == "*" and B[j+1][C] == "B":
            B[j+2][C] = "B"
            B[j+1][C] = " "
            Direc = random.randint(1,3)
            if Direc == 1:
                x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
            elif Direc == 2:
                x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
        elif B[j][C] == "*" and B[j+1][C] == "S":
            B[j+1][C] = "*" 
            B[j][C] = " "
            imprimir(a,B)
            sleep(Speed)
            system("cls")
            x = j + 1 
            y = C
            Pisadas += 1
        elif B[j][C] == "*" and B[j+1][C] == "E":
            B[j+1][C] = " "
            T = random.randint(0,1)
            if T == 0:
                B[j][C] = " "
                B[0][0] = "*"
                x = 0
                y = 0
                return x,y,Pisadas
            elif T == 1:
                B[j][C] = " "
                B[len(a)-1][len(a)-1] = "*"
                x = len(a)-1
                y =len(a)-1
                return x,y,Pisadas
        elif B[j][C] == "*" and B[j+1][C] == "F":
            Pisadas = 5000 
        else:
                x = len(a)-1
                y = C
    return x,y,Pisadas

#Función que mueve la particula hacia arriba de forma diagonal izquierda y revisa si la siguiente casilla es una letra o no
def Diagonal_Izquierda_Arriba(a, B, F, C,Pisadas):

    for j in range(len(a)-1,0,-1):
        for k in range(len(a)-1,0,-1):
            if B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == " ":
                B[F-1][C-1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F - 1
                C = C - 1
                Pisadas += 1
            elif B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == "A":
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
            elif B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == "B":
                B[j-2][k-2] = "B"
                B[j-1][k-1] = " "
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
            elif B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == "S":
                B[F-1][C-1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F - 1
                C = C - 1
                Pisadas += 1
            elif B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == "E":
                B[j-1][k-1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            elif B[j][k]== "*" and F != 0 and C != 0 and B[j-1][k-1] == "F":
                Pisadas = 5000
            else:
                x = 0 
                y = 0
    return F,C,Pisadas

#Función que mueve la particula hacia abajo de forma diagonal derecha y revisa si la siguiente casilla es una letra o no
def Diagonal_Derecha_Abajo(a, B, F, C,Pisadas):

    for j in range(len(a)):
        for k in range(len(a)-1):
            if (B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == " "):
                B[F+1][C+1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F + 1
                C = C + 1
                Pisadas += 1
            elif B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == "A":
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
            elif B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == "B":
                B[j+2][k+2] = "B"
                B[j+1][k+1] = " "
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
            elif B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == "S":
                B[F+1][C+1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F + 1
                C = C + 1
                Pisadas += 1
            elif B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == "E":
                B[j+1][k+1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            elif B[j][k]== "*" and F != len(a)-1 and C != len(a)-1 and B[j+1][k+1] == "F":
                Pisadas = 5000
            else:
                x = len(a)-1 
                y = len(a)-1
    return F,C,Pisadas

#Función que mueve la particula hacia arriba de forma diagonal derecha y revisa si la siguiente casilla es una letra o no
def Diagonal_Derecha_Arriba(a, B, F, C,Pisadas):
    
    for j in range (len(a)-1,0,-1):
        for k in range(len(a)-1):
            if B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == " ":
                B[F-1][C+1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F - 1 
                C = C + 1
                Pisadas += 1
            elif B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == "A":
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
                
            elif B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == "B":
                B[j-2][k+2] = "B"
                B[j-1][k+1] = " "
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == "S":
                B[F-1][C+1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F - 1 
                C = C + 1
                Pisadas += 1
            elif B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == "E":
                B[j-1][k+1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            elif B[j][k] == "*" and F != 0 and C != len(a)-1 and B[j-1][k+1] == "F":
                Pisadas = 5000
            else:
                x = 0 
                y = len(a)-1
    return F,C,Pisadas

#Función que mueve la particula hacia abajo de forma diagonal izquierda y revisa si la siguiente casilla es una letra o no
def Diagonal_Izquierda_Abajo(a, B, F, C,Pisadas):  

    for j in range (len(a)):
        for k in range(len(a)-1,0,-1):
            if B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == " ":
                B[F+1][C-1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F + 1
                C = C - 1
                Pisadas += 1
            elif  B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == "A":
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
            elif  B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == "B":
                B[j+2][k-2] = "B"
                B[j+1][k-1] = " "
                Direc = random.randint(1,3)
                if Direc == 1:
                    x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
                elif Direc == 2:
                    x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
                elif Direc == 3:
                    x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
            elif  B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == "S":
                B[F+1][C-1] = "*"
                B[F][C] = " "
                imprimir(a,B)
                sleep(Speed)
                system("cls")
                F = F + 1
                C = C - 1
                Pisadas += 1
            elif  B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == "E":
                B[j-1][k-1] = " "
                T = random.randint(0,1)
                if T == 0:
                    B[j][k] = " "
                    B[0][0] = "*"
                    x = 0
                    y = 0
                    return x,y,Pisadas
                elif T == 1:
                    B[j][k] = " "
                    B[len(a)-1][len(a)-1] = "*"
                    x = len(a)-1
                    y = len(a)-1
                    return x,y,Pisadas
            elif  B[j][k] == "*" and F != len(a)-1 and C != 0 and B[j+1][k-1] == "F":
                Pisadas = 5000
            else:
                x = len(a)-1 
                y = 0
    return F,C,Pisadas

# Funcion que la particula y decide la direccion de forma aleatoria si choca con una pared o si esta en una esquina
# respectiva

def movparticula(a,B,x,y,Inicio,Pisadas):

    #Condiciones que determina el primer movimiento segun la direccion que tome inicialmente
    #De forma aleatoria
    if Inicio == 0:
        # Variable que se asigna un numero aleatorio entre 1 y 8, segun el valor que tome decidira
        # el camino correcto de la matriz
        Q = random.randint(1,8)
        Inicio = 1
        if Q == 1:
            x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
        elif Q == 2:
            x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
        elif Q == 3:
            x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
        elif Q == 4:
            x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
        elif Q == 5:
            x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
        elif Q == 6:
            x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
        elif Q == 7:
            x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
        elif Q == 8:
            x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
        
    
#Ciclo que que dura hasta que supere el contador de pasos o la particula colicione con una F
    while Pisadas < 5000 :
    #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Esquina Izquierda Superior de la matriz
        if  x == 0 and y == 0 and y != len(a)-1 and x != len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
    #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Esquina Derecha Superior de la matriz
        elif x == 0 and y == len(a)-1 and y != 0 and x != len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
         #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar       
        # Esquina Derecha Inferior de la matriz
        elif x == len(a)-1 and y == len(a)-1 and x != 0  and y != 0:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
                #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Esquina Izquierda Inferior de la matriz
        elif x == len(a)-1 and y == 0 and x != 0 and y != len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
                #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Pared Izquierda de la matriz
        if x != 0 and y == 0 and x != len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Derecha(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
                #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Pared Derecha de la matriz
        elif x != 0 and x != len(a)-1 and y == len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Horizontal_Izquierda(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
                #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Pared Arriba de la matriz
        elif x == 0 and y != 0 and y != len(a)-1:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Abajo(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Abajo(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Abajo(a, B, x, y,Pisadas)
                #Si la particula se encuentra dentro de estas posicionas, esta elijira entre una de las 
    #tres posibles direcciones y realizara la funcion de rebotar
        # Pared Abajo de la matriz
        elif x == len(a)-1 and y != 0 and y != 0:
            # Variable que indicara la direccion que tomara la particula hasta llegar este punto
            Direc = random.randint(1,3)
            if Direc == 1:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Derecha_Arriba(a, B, x, y,Pisadas)
            elif Direc == 2:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Vertical_Arriba(a, B, x, y,Pisadas)
            elif Direc == 3:
                #LLamado de la funcion si la condicion es verdadera
                x,y,Pisadas = Diagonal_Izquierda_Arriba(a, B, x, y,Pisadas)
                
#Funcion principal el cual llama a todas las otras funciones en el orden correcto 
def main():

    
    lista = fil_col()
    F = random.randint(0,len(lista)-1)
    C = random.randint(0,len(lista)-1)
    Contador = 0
    Matriz = inicializacion(lista)
    Sentido = random.choice([-1,1])
    Direc = random.choice(["H"])
    particula(lista,Matriz,F,C)
    Letras(lista,Matriz,F,C)
    #x,y = PosParticula(lista,Matriz,F,C)
    x = F
    y = C
    Inicio = 0
    Pisadas = 0
    movparticula(lista,Matriz,x,y,Inicio,Pisadas)
   
    print("***********************************************************************")
    print("*********************  Gracias por jugar  *****************************")
    print("***********************************************************************")


if __name__ == "__main__":
    main()
